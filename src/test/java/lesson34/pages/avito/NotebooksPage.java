package lesson34.pages.avito;

import com.codeborne.selenide.Condition;
import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.Selenide;
import com.codeborne.selenide.SelenideElement;

public class NotebooksPage {
    public static final String categoryId = "98";
    public static final String categoryText = "Ноутбуки";

    private final SelenideElement searchForm = Selenide.$x("//div[@data-marker='search-form']");
    private final SelenideElement categorySelect = searchForm.$x(".//select[@id='category']");
    private final SelenideElement searchFilters = Selenide.$x("//div[@data-marker='search-filters']");
    private final SelenideElement saleItemsArea = Selenide.$x("//div[@data-marker='catalog-serp']");


    public NotebooksPage validatePage()  {
        searchForm.shouldBe(Condition.visible);
        categorySelect
            .shouldBe(Condition.visible)
            .getSelectedOption()
            .should(Condition.attribute("value", categoryId))
            .should(Condition.text(categoryText));

        return this;
    }

    public NotebooksPage setProducer(Producer producer)  {
        searchFilters.shouldBe(Condition.visible);

        searchFilters.$x(".//span[@data-marker='params[112916](" + producer.code + ")/text']")
            .shouldBe(Condition.exist)
            .click();

        return this;
    }

    public NotebooksPage setPriceRange(int from, int to)  {
        searchFilters.shouldBe(Condition.visible);

        searchFilters.$x(".//input[@data-marker='price/from']")
            .shouldBe(Condition.exist)
            .sendKeys("" + from);

        searchFilters.$x(".//input[@data-marker='price/to']")
            .shouldBe(Condition.exist)
            .sendKeys("" + to);

        return this;
    }

    public NotebooksPage runSearchFilters()  {
        searchFilters.shouldBe(Condition.visible);

        searchFilters.$x(".//button[@data-marker='search-filters/submit-button']")
            .shouldBe(Condition.exist)
            .click();

        return this;
    }

    public ElementsCollection getSaleItems()  {
        saleItemsArea.shouldBe(Condition.visible);

        return saleItemsArea.$$x(".//div[@data-marker='item']");
    }

    public enum Producer {
        Lenovo("841331", "lenovo"),
        Apple("841338", "macbook"),
        HP("841312", "hp"),
        MSI("841306", "msi");

        private String code;
        private String searchText;

        Producer(String code, String searchText) {
            this.code = code;
            this.searchText = searchText;
        }

        public String getSearchText() {
            return searchText;
        }
    }

}
