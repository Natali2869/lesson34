package lesson34.pages.avito;

import com.codeborne.selenide.Condition;
import com.codeborne.selenide.Selenide;
import com.codeborne.selenide.SelenideElement;

public class MainPage {

    private static final String DOMAIN = "https://www.avito.ru";
    private final SelenideElement searchForm = Selenide.$x("//div[@data-marker='search-form']");
    private final SelenideElement notebooksCategory = searchForm.$x(".//select[@id='category']/option[@value='"
                                                                     + NotebooksPage.categoryId + "' and text()"
                                                                     + "='" + NotebooksPage.categoryText + "']");

    public MainPage open() {
        Selenide.open(DOMAIN);
        Selenide.open("https://www.yandex.ru");
        Selenide.open(DOMAIN);

        return this;
    }

    public MainPage validatePage()  {
        searchForm.shouldBe(Condition.visible);
        notebooksCategory.shouldBe(Condition.visible);

        return this;
    }

    public NotebooksPage toNotebooksCategory() {
        notebooksCategory.click();

        return new NotebooksPage();
    }

}
