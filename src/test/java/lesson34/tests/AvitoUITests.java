package lesson34.tests;

import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.SelenideElement;
import lesson34.pages.avito.MainPage;
import lesson34.pages.avito.NotebooksPage;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

public class AvitoUITests extends UITestsBase {
    private final static NotebooksPage.Producer PRODUCER = NotebooksPage.Producer.Lenovo;
    private final static Integer PRICE_FROM = 15000;
    private final static Integer PRICE_TO = 25000;

    @DisplayName("Тест фильтров avito по поиску ноутбуков (Электроника -> Ноутбуки)")
    @Test
    public void avitoNotebooksFilterTest() {

        ElementsCollection saleItems = new MainPage()
            .open()
            .validatePage()
            .toNotebooksCategory()
            .validatePage()
            .setProducer(PRODUCER)
            .setPriceRange(PRICE_FROM, PRICE_TO)
            .runSearchFilters()
            .validatePage()
            .getSaleItems();

        int countValidated = 0;
        for (SelenideElement saleItem : saleItems) {
            boolean fullyValidated = validateSaleItem(saleItem, PRODUCER, PRICE_FROM, PRICE_TO);
            if (fullyValidated) {
                countValidated++;
            }
        }
        double fullyValidatedRatio = (double) countValidated / saleItems.size();
        System.out.println(String.format("fully validated sale items ratio is %s out of %s items", fullyValidatedRatio, saleItems.size()));
        Assertions.assertTrue(fullyValidatedRatio > 0.5);
    }

    private boolean validateSaleItem(SelenideElement saleItem, NotebooksPage.Producer producer, int priceFrom, int priceTo) {
        String title = saleItem.$x(".//h3[@itemprop='name']").getText();
        String description = saleItem.$x(".//meta[@itemprop='description']").getAttribute("content");
        String price = saleItem.$x(".//span[@data-marker='item-price']/meta[@itemprop='price']").getAttribute("content");

        System.out.println(String.format("validating sale item title=%s, price=%s", title, price));

        boolean producerFound =
            (title != null && title.toLowerCase().contains(producer.getSearchText()))
                ||
                (description != null && description.toLowerCase().contains(producer.getSearchText()));

        boolean priceChecked = false;
        if (price != null && price.matches("\\d+")) {
            priceChecked = true;
            Assertions.assertTrue(priceFrom <= Integer.parseInt(price));
            Assertions.assertTrue(priceTo >= Integer.parseInt(price));
        }

        return producerFound && priceChecked;
    }
}
